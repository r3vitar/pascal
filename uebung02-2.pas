PROGRAM PrimeCalculator;
uses DateUtils, sysutils;
VAR
  FromTime, ToTime: TDateTime;
  input, i: LongInt;
  result: Boolean;
  DiffMinutes: Integer;
FUNCTION IsPrime(num: LongInt): Boolean;
VAR
  prime: Boolean;
  i: LongInt;
BEGIN (* isPrime *)
  IF ((num > 1) AND (num <= 3) ) THEN BEGIN
    prime := true;
  END ELSE IF ((num <= 1) OR (num MOD 2 = 0)) THEN BEGIN
    prime := false;
  END ELSE BEGIN
    i:=3;
    prime := true;
    FOR i := 3 TO Round(Sqrt(num)) DO BEGIN
      IF (num MOD i = 0) THEN BEGIN
        prime := false;
        Break;
      END; (* IF *)
    END; (* FOR *) 
  END; (* IF *)
  IsPrime := prime;
END; (* isPrime *)
BEGIN (* PrimeCalculator *)
  input := 131071;
  FromTime := Now;
  FOR i := 0 TO input DO BEGIN
      result := IsPrime(i);
  END; (* FOR *)
  ToTime := Now;
  DiffMinutes := MilliSecondsBetween(ToTime,FromTime);
  WriteLn('ist ', input, ' eine primzahl? ', result);
  WriteLn(DiffMinutes);

END. (* PrimeCalculator *)