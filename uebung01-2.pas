(*FREIWILLIG*)
(*only works on numbers*)
(*it treats all negative numbers as positive numbers*)
PROGRAM Bildschirmzeit;
VAR
  timeLimit, sumTime, input, daysAboveLimitCnt: INTEGER;
BEGIN
  sumTime := 0;
  daysAboveLimitCnt := 0;
  Write('Zeitlimit: ');
  Read(timeLimit);
  WriteLn('Bitte geben Sie die taeglichen Zeiten ein... (eine Zeit pro Zeile und mit "0" kann abgebrochen werden)');
  REPEAT
    Write('Taegliche Zeiten: ');
    Read(input);
    IF not(input = 0) THEN BEGIN
      sumTime := sumTime + Abs(input);
      IF (sumTime > timeLimit) THEN BEGIN
        daysAboveLimitCnt := daysAboveLimitCnt + 1;
      END;
    END;
  UNTIL (input = 0);
  WriteLn('Gesamtzeit: ', sumTime);
  IF (daysAboveLimitCnt > 0) THEN BEGIN
    WriteLn('Zeitueberschreitung: ', (sumTime - timeLimit));
  END ELSE BEGIN
    WriteLn('Zeitueberschreitung: ', 0)
  END;
  WriteLn('Tage ueber Limit: ', daysAboveLimitCnt);
END.