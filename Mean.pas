(*only works on numbers*)
(*it treats all negative numbers as positive numbers*)
PROGRAM Mean;
VAR
  count: INTEGER;
  input, sum: INTEGER;
  avarage: REAL;
BEGIN
  count := 0;
  sum := 0;
  WriteLn('Pls input numbers for MEAN (exit with "0")');
  REPEAT
    Read(input);
    IF not(input = 0) THEN BEGIN
      sum := sum + Abs(input);
      count := count + 1;
    END; (* IF *)
  UNTIL (input = 0); (* REPEAT *)
  IF not(count = 0) THEN BEGIN
    avarage := sum / count;
  END; (* IF *)
  Write('Avarage: ');
  WriteLn(avarage:0:3);
  WriteLn('end')
END.