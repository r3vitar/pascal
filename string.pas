PROGRAM Strings;

VAR 
  t:String;
  hgb:String;
  res:String;
    
BEGIN

 t:= 'FH';
 hgb:= 'Hagenberg';
 res:=t+' '+hgb+'!';
 WriteLn (res);
 WriteLn (Length(res));
 WriteLn (ORD(res[0]));

END.(*Strings*)