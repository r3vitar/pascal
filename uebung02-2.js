const process = require('process');


function isPrime2(num) {
    if (num > 1 && num <= 3) {
        return true;
    } 
    const div2 = num / 2;
    if (num < 1 || div2 === ~~(div2)) {
        return false;
    }

    let prime = true;

    for(let i = 3; i <= Math.sqrt(num)  && prime; i+=1) {
        const iWasNedWarumDesSchnellaIsOisModulu = num / i;
        if(iWasNedWarumDesSchnellaIsOisModulu === ~~(iWasNedWarumDesSchnellaIsOisModulu)) {
            prime = false;
        }
    }

    return prime;
}

const prims = [];

const max = 131071//1299710
const start = process.hrtime.bigint();
for (let i = 2; i <= max; i++) {
    if(isPrime2(i)) {
        prims.push(i)
    }
}
const duration = process.hrtime.bigint() - start;
let highest = prims[prims.length-1];

console.log({highest, count: prims.length, dur: Number(duration)/1e6})