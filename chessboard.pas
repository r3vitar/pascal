PROGRAM Chessboard;
  TYPE
    TPlayer = (
      NONE,
      BLACK,
      WHITE
    )
    TChessFigure = 
      (
        EMPTY,
        PAWN, (* Geringverdiener *)
        KNIGHT, (*Springer*)
        BISHOP, (*Laeufer*)
        ROOK, (*Turm*)
        QUEEN, (*Dame*)
        KING (*Dame*)
      );
  VAR
    figure: TChessFigure;
BEGIN (* Chessboard *)
  figure := nil;

  FOR figure := LOW(figure) TO HIGH(figure) DO BEGIN
    WriteLn('curr figure = ', figure);
  END; (* FOR *)

  WriteLn(figure);
END. (* Chessboard *)