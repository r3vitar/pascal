PROGRAM FAK;
VAR
  input, i: INTEGER;
  sum: LongInt;
BEGIN (* FAK *)
  Write('Fakultaet von was?: ');
  Read(input);
  sum := input;
  FOR i := (input - 1) downto 1 DO BEGIN
    sum := sum * i;
  END; (* FOR *)

  WriteLn(input, '! = ',sum);
END. (* FAK *)