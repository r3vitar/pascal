PROGRAM SameNumbers;
FUNCTION DifferentNumbersCount(num0, num1, num2: INTEGER): INTEGER;
BEGIN (* DifferentNumbersCount *)
  IF (num0 = num1) AND (num1 = num2) THEN BEGIN
    DifferentNumbersCount := 1;
  END ELSE IF (num0 = num1) OR (num0 = num2) OR (num1 = num2) THEN BEGIN
    DifferentNumbersCount := 2;
  END ELSE BEGIN
    DifferentNumbersCount := 3;  
  END;
END; (* DifferentNumbersCount *)
BEGIN (* SameNumbers *)
  WriteLn('Eingabe: ', 5, ', ', 7, ', ',9, ' = ', DifferentNumbersCount(5, 7, 9), ' verschiedene Nummern');
  WriteLn('Eingabe: ', 5, ', ', 7, ', ',5, ' = ', DifferentNumbersCount(5, 7, 5), ' verschiedene Nummern');
  WriteLn('Eingabe: ', 7, ', ', 7, ', ',7, ' = ', DifferentNumbersCount(7, 7, 7), ' verschiedene Nummern');
END. (* SameNumbers *)
