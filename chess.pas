PROGRAM Chess;

TYPE
  TChessFigure =
    (
      EMPTY, (*leeres Feld*)
      PAWN,(*Bauer*)
      KNIGHT, (*Springer*)
      BISHOP, (*Läufer*)
      ROOK, (*Turm*)
      QUEEN, (*Dame*)
      KING  (*König*)
    );
    
    TPlayer = 
     (
      NONE, 
      BLACK,
      WHITE
     );

    TSquare = RECORD
      Figure: TChessFigure;
      Player: TPlayer;
    END;
    TFiles = 'A'..'H'; (*8 Elemente*)
    TRanks = 1..8;      (*8 Elemente*)
    TChessBoard = ARRAY [TFiles] OF
                  ARRAY [TRanks] OF 
                  TSquare;
                  
  TArray = ARRAY [TFiles] OF INTEGER;
 

PROCEDURE InitArray(VAR arr:ARRAY OF INTEGER);
VAR 
  i:INTEGER;
BEGIN
  FOR i := 0 TO HIGH(arr) DO BEGIN
    arr[i] := 42;

END; 
END;
  
PROCEDURE PrintArray(arr:ARRAY OF INTEGER);
VAR 
  i:INTEGER;
BEGIN
  FOR i := 0 TO HIGH(arr) DO BEGIN
    Write(arr[i], ' ');
  END;
  WriteLn;
END;
VAR
  figure: TChessFigure;
  
  chessBoard: TChessBoard;
  
  arr: TArray;
  c: CHAR;
  i: INTEGER;
  
  BEGIN
  
    WriteLn(SizeOf(TSquare));
    WriteLn(SizeOf(TChessFigure));
    WriteLn(SizeOf(TPlayer));
    WriteLn(SizeOf(chessBoard));
    
    FOR c:= LOW(chessBoard) TO HIGH(chessBoard) DO BEGIN
      (*c->A*)
      FOR i:= LOW(chessBoard[c]) TO HIGH(chessBoard[c]) DO BEGIN
        (*i->1*)
        chessBoard[c][i].Figure := EMPTY;
        chessBoard[c][i].Player := NONE;
        WriteLn('chessBoard[', c, '][', i, ']-> (',
        chessBoard[c][i].Figure, '/' ,chessBoard[c][i].Player,')');
      END;
    END;
    
    arr['A'] :=3;
    WriteLn(arr['A']);
    FOR c := LOW(arr) TO HIGH(arr) DO BEGIN
      arr[c] := 0;
      WriteLn('arr[' , c, '] ->', arr[c]);
    END;
    
    InitArray(arr);
    PrintArray(arr);
    figure :=PAWN;
    WriteLn(figure);
    WriteLn(ORD(figure));
    WriteLn(ORD(HIGH(figure)));
    
    FOR figure := LOW(TChessFigure) TO HIGH(TChessFigure) DO BEGIN
     WriteLn('Current figure type: ',figure);
    END;
    
    WriteLn(SUCC(ROOK));
    WriteLn(PRED(KING));
    
    IF figure = KING THEN BEGIN 
      WriteLn('TRUE!');
    END;
    
    WriteLn(TChessFigure(1));
    WriteLn(INTEGER(KING));
  
  
  END.(*Chessboard*)