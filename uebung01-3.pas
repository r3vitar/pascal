(*FREIWILLIG*)
(*only works on numbers*)
(*it treats all negative numbers as positive numbers*)
PROGRAM Zeitrechnung;
VAR
  h, m, s: INTEGER;
  hTemp, mTemp, sTemp: INTEGER;
BEGIN

  WriteLn('Zeitspanne 1');
  Write('h: ');
  Read(h);
  Write('m: ');
  Read(m);
  Write('s: ');
  Read(s);

  WriteLn('Zeitspanne 2');
  Write('h: ');
  Read(hTemp);
  Write('m: ');
  Read(mTemp);
  Write('s: ');
  Read(sTemp);

  s := s + abs(sTemp);
  m := m + (s div 60);
  s := s mod 60;

  m := m + abs(mTemp);
  h := h + (m div 60);
  m := m mod 60;

  h := h + abs(hTemp);

  WriteLn('Summe: ', h, ':', m, ':', s);
END.