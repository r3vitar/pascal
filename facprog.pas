PROGRAM FactProg;
  VAR
    x: INTEGER;
    res: LongInt;
  PROCEDURE FactProc(x: INTEGER; VAR res: LongInt);
    VAR
      i, fact: INTEGER;
    BEGIN (* FactProc *)
      fact := 1;
      FOR i := 1 TO x DO BEGIN
        fact := fact * i;
      END; (* FOR *)
      res := fact;
    END; (* FactProc *)
  FUNCTION FactFunc(x: INTEGER): LongInt;
    VAR
      i, fact: INTEGER;
    BEGIN (* FactFunc *)
      fact := 1;
      FOR i := 1 TO x DO BEGIN
        fact := fact * i;
      END; (* FOR *)

      FactFunc := fact;
    END; (* FactFunc *)
BEGIN (* FacProg *)
  x:= 3;
  FactProc(x, res);
  WriteLn('proc: ',x, '! = ', res);
  WriteLn('func: ',x, '! = ', FactFunc(x))

END. (* FacProg *)