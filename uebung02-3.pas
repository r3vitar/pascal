PROGRAM Armstrong;
VAR input: INTEGER;
  FUNCTION Power(num, powerTo: INTEGER): LongInt;
    VAR
      i: INTEGER;
      result: LongInt;
    BEGIN
      result := num;
      IF (powerTo = 0) THEN BEGIN
        result := 1;
      END ELSE BEGIN
        FOR i := 1 TO powerTo DO BEGIN
          result := result * (num);
        END; (* FOR *)
      END; (* IF *)
      Power := result;
  END;
  FUNCTION CountDigits(num: LongInt): Integer;
  VAR
    i, count: Integer;
  BEGIN (* CountDigits *)
    count := 0;
    i := 0;
    WHILE count = 0 DO BEGIN
      IF (num div Power(10, i)) = 0 THEN BEGIN
        count := i+1;
      END ELSE BEGIN
        i:= i+1;
      END; (* IF *)
    END; (* WHILE *)
    CountDigits := count;
  END; (* CountDigits *)
  FUNCTION GetXNumber(num, x: LongInt): Integer;
  BEGIN (* GetXNumber *)
    GetXNumber := (num div Power(10, x-1)) mod 10;
  END; (* GetXNumber *)
  FUNCTION IsArmstrongNumber(num: LongInt): Boolean;
  VAR
    i, count: Integer;
    armstrong: LongInt;
  BEGIN (* IsArmstrongNumber *)
    count := CountDigits(num);
    armstrong := 0;
    FOR i := 0 TO count-1 DO BEGIN
      armstrong := armstrong + Power(GetXNumber(num, i), count-1);
    END; (* FOR *)
    IsArmstrongNumber := armstrong = num;
  END; (* IsArmstrongNumber *)
BEGIN (* Armstrong *)
  Read(input);
  IF (IsArmstrongNumber(input)) THEN BEGIN
    WriteLn(input, ' ist Armstrong');
  END ELSE BEGIN
    WriteLn(input, ' ist nicht Armstrong');
  END; (* IF *)
END. (* Armstrong *)