(*only works on numbers*)
(*it treats all negative numbers as positive numbers*)
PROGRAM MeanWhile;
VAR
  count: INTEGER;
  input, sum: INTEGER;
  avarage: REAL;
BEGIN
  count := 0;
  sum := 0;
  WriteLn('Pls input numbers for MEAN (exit with "0")');
  Write('Input: ');
  Read(input);

  WHILE (input <> 0) DO BEGIN
      sum := sum + Abs(input);
      count := count + 1;
      Write('Input: ');
      Read(input);
  END; (* WHILE *)
  
  IF not(count = 0) THEN BEGIN
    avarage := sum / count;
  END; (* IF *)
  WriteLn('Avarage: ');
  WriteLn(' ______');
  Write('< ');
  Write(avarage:2:2);
  WriteLn(' >');
  WriteLn(' ------');
  WriteLn('        \   ^__^');
  WriteLn('         \  (oo)\_______    ');
  WriteLn('            (__)\       )\/\');
  WriteLn('                ||----w |  ');
  WriteLn('                ||     ||   ');
  WriteLn('');

END.