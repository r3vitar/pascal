(* HelloWorld:                                                               *)
(* -----------                                                               *)
(* This is a simple hello world program to test the VS Code workspace.       *)
(* ========================================================================= *)
PROGRAM HelloWorld;
VAR
  dogAge: real;
  dogAge2: real;
BEGIN (* HelloWorld *)
  dogAge := ReadLn();
  WriteLn(16 * ln(dogAge) + 31);
END. (* HelloWorld *)
