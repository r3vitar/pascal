PROGRAM TypesTest;
  TYPE
    IntegerSubRange = 1..10;
  VAR
    i, j: QWord;
    is1: IntegerSubRange;
    bool1: BOOLEAN;
BEGIN (* TypesTest *)


  is1 := 2;

  i := 3;
  WriteLn(Succ(i));
  WriteLn(Pred(i));
  WriteLn(Ord(i));
  WriteLn(Low(i));
  WriteLn(High(i));
  Inc(i);
  WriteLn(i);
  Dec(i);
  WriteLn(i);
  WriteLn(LONGINT(@i));
  WriteLn(SizeOf(i));
  WriteLn(LONGINT(@j));
  WriteLn(LONGINT(@i + 16));


END. (* TypesTest *)