PROGRAM BCPROG;
VAR
  x, y: INTEGER;
  res: REAL;
  FUNCTION FactFunc(x: INTEGER): LongInt;
    VAR
      i, fact: INTEGER;
    BEGIN (* FactFunc *)
      fact := 1;
      FOR i := 1 TO x DO BEGIN
        fact := fact * i;
      END; (* FOR *)

      FactFunc := fact;
    END; (* FactFunc *)
  FUNCTION BC(n,k :INTEGER): REAL;
  BEGIN (* BC *)
    BC := FactFunc(n) / (FactFunc(k) * FactFunc((n - k)))
  END; (* BC *)
BEGIN (* BCPROG *)

Write('n: ');
Read(x);
Write('k: ');
Read(y);
res := BC(x, y);
WriteLn(res:5:5);
  
END. (* BCPROG *)